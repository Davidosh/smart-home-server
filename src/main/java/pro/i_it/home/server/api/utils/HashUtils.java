package pro.i_it.home.server.api.utils;

import jonelo.jacksum.JacksumAPI;
import jonelo.jacksum.algorithm.AbstractChecksum;

import java.security.NoSuchAlgorithmException;

public class HashUtils {

    public static String getPasswordHash(String password) {
        AbstractChecksum checksum = null;
        try {
            checksum = JacksumAPI.getChecksumInstance("whirlpool");
            checksum.update(password.getBytes());
        } catch (NoSuchAlgorithmException nsae) {
            return null;
        }
        return checksum.getFormattedValue();
    }
}
