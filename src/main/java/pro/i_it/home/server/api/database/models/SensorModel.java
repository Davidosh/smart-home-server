package pro.i_it.home.server.api.database.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class SensorModel implements Serializable {
    @Expose
    private int id;
    @Expose
    private String deviceId;
    @Expose
    private int deviceTypeId;
    @Expose
    private String lastServerIp;
    @Expose
    private boolean enabled;
    @Expose
    private String description;

    public SensorModel() {
    }

    public SensorModel(int id, String deviceId, int deviceTypeId, String lastServerIp, boolean enabled, String description) {
        this.id = id;
        this.deviceId = deviceId;
        this.deviceTypeId = deviceTypeId;
        this.lastServerIp = lastServerIp;
        this.enabled = enabled;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public SensorModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public SensorModel setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public int getDeviceTypeId() {
        return deviceTypeId;
    }

    public SensorModel setDeviceTypeId(int deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
        return this;
    }

    public String getLastServerIp() {
        return lastServerIp;
    }

    public SensorModel setLastServerIp(String lastServerIp) {
        this.lastServerIp = lastServerIp;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public SensorModel setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SensorModel setDescription(String description) {
        this.description = description;
        return this;
    }
}
