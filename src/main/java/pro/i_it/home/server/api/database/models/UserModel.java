package pro.i_it.home.server.api.database.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class UserModel implements Serializable {

    @Expose
    private int id;
    @Expose
    private String nickname;
    @Expose(serialize = false)
    private String password;

    public UserModel(int id, String nickname, String password) {
        this.id = id;
        this.nickname = nickname;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
