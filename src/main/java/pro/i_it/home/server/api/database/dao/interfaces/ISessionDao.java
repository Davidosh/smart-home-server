package pro.i_it.home.server.api.database.dao.interfaces;

import pro.i_it.home.server.api.database.entities.HttpSessionDbModel;

public interface ISessionDao {
    HttpSessionDbModel createSession(int userId);

    int deleteSession(String uuid);

    boolean isSessionExist(String uuid);
}
