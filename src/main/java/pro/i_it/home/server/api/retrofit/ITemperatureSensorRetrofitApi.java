package pro.i_it.home.server.api.retrofit;

import io.reactivex.Single;
import pro.i_it.home.server.api.retrofit.responces.TemperatureResponceRaspberryModel;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ITemperatureSensorRetrofitApi {
    @GET("/pin")
    Single<TemperatureResponceRaspberryModel> getTemp(@Query("deviceType") int deviceType);
}
