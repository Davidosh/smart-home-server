package pro.i_it.home.server.api.retrofit.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SwitchLedResponceRaspberryModel {
    @Expose
    @SerializedName("responceCode")
    private int responceCode;

    @Expose
    @SerializedName("data")
    private Data data;

    public int getResponceCode() {
        return responceCode;
    }

    public Data getData() {
        return data;
    }

    public static class Data {
        @Expose
        @SerializedName("turnOn")
        private boolean turnOn;


        public boolean isTurnOn() {
            return turnOn;
        }
    }
}
