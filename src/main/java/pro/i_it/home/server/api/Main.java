package pro.i_it.home.server.api;

import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import pro.i_it.home.server.api.scoket.SocketTestServer;
import pro.i_it.home.server.api.web.servlets.AuthServlet;
import pro.i_it.home.server.api.web.servlets.DeviceServlet;
import pro.i_it.home.server.api.web.servlets.TestServlet;

public class Main {
    private static SocketTestServer socketTestServer;

    public static void main(String[] args) throws Exception {

        //start socket
        socketTestServer = new SocketTestServer(8090);

        //start REST
        ServletHandler servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(TestServlet.class, "/");
        servletHandler.addServletWithMapping(AuthServlet.class, "/auth");
        servletHandler.addServletWithMapping(DeviceServlet.class, "/device");


        Server server = new Server(new QueuedThreadPool(4, 1));
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory());

        connector.setPort(8080);
        server.addConnector(connector);
        server.setHandler(servletHandler);
        server.start();
        server.join();
    }
}
