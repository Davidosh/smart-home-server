package pro.i_it.home.server.api.scoket.packets.message;

import pro.i_it.home.server.api.scoket.packets.interfaces.OutPacket;

import java.io.DataOutputStream;
import java.io.IOException;

public class MessagePacketOut implements OutPacket {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void sendA(DataOutputStream outputStreamWriter) {
        try {
            outputStreamWriter.writeInt(OutPacket.PACKAGE_A);
            outputStreamWriter.writeUTF("David");
            outputStreamWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendB(DataOutputStream outputStreamWriter) {
        try {
            outputStreamWriter.writeInt(OutPacket.PACKAGE_B);
            outputStreamWriter.writeUTF("Eugene");
            outputStreamWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
