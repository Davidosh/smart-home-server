package pro.i_it.home.server.api.database.dao;

import io.reactivex.Completable;
import io.reactivex.Single;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pro.i_it.home.server.api.database.dao.interfaces.ISensorDao;
import pro.i_it.home.server.api.database.entities.SensorDbModel;
import pro.i_it.home.server.api.database.models.SensorModel;
import pro.i_it.home.server.exceptions.DbItemNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class SensorDao implements ISensorDao {
    private SessionFactory sessionFactory;

    public SensorDao(SessionFactory session) {
        this.sessionFactory = session;
    }

    @Override
    public Completable insertSensor(SensorModel sensorModel) {
        return Completable.fromAction(() -> {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            SensorDbModel sensorDbModel = new SensorDbModel(sensorModel.getDeviceId(),
                    sensorModel.getDeviceTypeId(),
                    sensorModel.getLastServerIp(),
                    sensorModel.isEnabled(),
                    sensorModel.getDescription());
            session.save(sensorDbModel);
            session.flush();
            session.getTransaction().commit();
            session.close();
        });
    }

    @Override
    public Completable deleteSensor(int id) {
        return Completable.fromAction(() -> {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            Query query = session.createQuery("delete DeviceTypeDbModel " +
                    "where id = :id");
            query.setParameter("id", id);
            session.flush();
            session.getTransaction().commit();
            session.close();
        });

    }

    @Override
    public Completable updateSensor(SensorModel sensorModel) {
        return Completable.fromAction(() -> {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            SensorDbModel sensorDbModel = new SensorDbModel(sensorModel.getDeviceId(),
                    sensorModel.getDeviceTypeId(),
                    sensorModel.getLastServerIp(),
                    sensorModel.isEnabled(),
                    sensorModel.getDescription());


            session.update(sensorDbModel);
            session.flush();
            session.getTransaction().commit();
            session.close();
        });
    }

    @Override
    public Single<SensorModel> getSensor(int id) {
        return Single.create(singleEmitter -> {
            Session session = sessionFactory.openSession();
            Query query = session.createQuery("select * from SensorDbModel a " +
                    "where a.id = :id");
            query.setParameter("id", id);
            SensorDbModel sensorDbModel = query.list().isEmpty() ? null : (SensorDbModel) query.list().get(0);
            session.close();
            if (sensorDbModel != null) {
                singleEmitter.onSuccess(new SensorModel()
                        .setId(sensorDbModel.getId())
                        .setDeviceId(sensorDbModel.getDeviceId())
                        .setDeviceTypeId(sensorDbModel.getDeviceTypeId())
                        .setLastServerIp(sensorDbModel.getLastServerIp())
                        .setEnabled(sensorDbModel.isEnabled())
                        .setDescription(sensorDbModel.getDescription()));
            } else {
                singleEmitter.onError(new DbItemNotFoundException());
            }
        });
    }

    @Override
    public Single<List<SensorModel>> getAllSensors() {
        return Single.create(singleEmitter -> {
            Session session = sessionFactory.openSession();
            Query query = session.createQuery("select * from SensorDbModel");
            List<SensorDbModel> sensorDbModels = query.list();
            List<SensorModel> sensorModels = new ArrayList<>();
            session.close();
            if (sensorDbModels.isEmpty()) {
                singleEmitter.onError(new DbItemNotFoundException());
            }
            for (SensorDbModel sensorDbModel : sensorDbModels) {
                sensorModels.add(new SensorModel()
                        .setId(sensorDbModel.getId())
                        .setDeviceId(sensorDbModel.getDeviceId())
                        .setDeviceTypeId(sensorDbModel.getDeviceTypeId())
                        .setLastServerIp(sensorDbModel.getLastServerIp())
                        .setEnabled(sensorDbModel.isEnabled())
                        .setDescription(sensorDbModel.getDescription()));
            }
            singleEmitter.onSuccess(sensorModels);
        });
    }
}
