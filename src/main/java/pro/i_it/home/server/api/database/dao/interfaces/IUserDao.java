package pro.i_it.home.server.api.database.dao.interfaces;

import pro.i_it.home.server.api.database.models.UserModel;

public interface IUserDao {
    UserModel getUserByUuidSession(String uuid);

    int getUserIdByLoginAndPassword(String login, String password);
}
