package pro.i_it.home.server.api.scoket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class SocketTestServer extends Thread {
    private ServerSocket serverSocket;
    private ClientManager clientManager;


    public SocketTestServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientManager = new ClientManager();
        start();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();
                TestClient client = new TestClient(socket);
                clientManager.addNewClient(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        clientManager.interrupt();
    }
}
