package pro.i_it.home.server.api.web.model.devices;

public class DeviceStatusModel {
    private int id;
    private boolean state;

    public int getId() {
        return id;
    }

    public DeviceStatusModel setId(int id) {
        this.id = id;
        return this;
    }

    public boolean isState() {
        return state;
    }

    public DeviceStatusModel setState(boolean state) {
        this.state = state;
        return this;
    }
}
