package pro.i_it.home.server.api.web.servlets;

import pro.i_it.home.server.api.Context;
import pro.i_it.home.server.api.interactors.AuthInteractor;
import pro.i_it.home.server.api.utils.HashUtils;
import pro.i_it.home.server.api.web.model.BaseResponceModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthServlet extends HttpServlet {

    private static final String PASSWORD = "password";

    private static final String LOGIN = "login";

    private static final String TYPE = "type";

    static final String SESSION_ID = "sessionId";

    private AuthInteractor authInteractor;

    public AuthServlet() {
        authInteractor = Context.getContext().getInteractorsManager().getAuthInteractor();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Thread name : " + Thread.currentThread().getName());
        if (Integer.parseInt(req.getParameter(TYPE)) == 1) {
            login(req, resp);
        } else {
            logout(req, resp);
        }
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getParameter(LOGIN) == null || req.getParameter(PASSWORD) == null) {
            resp.getWriter().append(new BaseResponceModel<>().setErrorDescription("No login or password").toString());
            return;
        }
        String sessionId = authInteractor.signIn(req.getParameter(LOGIN), HashUtils.getPasswordHash(req.getParameter(PASSWORD)));
        resp.getWriter().append(sessionId == null ?
                new BaseResponceModel<>().setErrorDescription("Invalid login or password").toString() :
                new BaseResponceModel<>(sessionId).toString());
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String sessionId = req.getParameter(SESSION_ID);
        if (authInteractor.signOut(sessionId) != 0) {
            resp.getWriter().append(new BaseResponceModel<>().toString());
        } else {
            resp.getWriter().append(new BaseResponceModel<>().setResponceCode(-1).toString());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
