package pro.i_it.home.server.api.scoket;

import pro.i_it.home.server.api.scoket.packets.interfaces.InPacket;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import static pro.i_it.home.server.api.scoket.packets.interfaces.OutPacket.PACKAGE_A;
import static pro.i_it.home.server.api.scoket.packets.interfaces.OutPacket.PACKAGE_B;

public class SocketClient {

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        Socket socket;
        try {
            socket = new Socket("localhost", 8090);
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            DataOutputStream oos = new DataOutputStream(socket.getOutputStream());
            DataInputStream ois = new DataInputStream(socket.getInputStream());
            System.out.println("Client connected to socket.");
            oos.writeInt(PACKAGE_A);
            oos.flush();

            while (!socket.isOutputShutdown()) {
                switch (ois.readInt()) {
                    case InPacket.PACKAGE_A:
                        System.out.println("PACKAGE A : " + ois.readUTF());
                        oos.writeInt(PACKAGE_B);
                        oos.flush();
                        break;
                    case InPacket.PACKAGE_B:
                        System.out.println("PACKAGE B : " + ois.readUTF());
                        oos.writeInt(PACKAGE_A);
                        oos.flush();
                        break;
                    default:
                        break;
                }
            }
            System.out.println("Closing connections & channels on clentSide - DONE.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}