package pro.i_it.home.server.api.web.model.devices;

public class LightSensorModel {
    private int id;
    private boolean isSwitchedOn;

    public LightSensorModel(int id, boolean isSwitchedOn) {
        this.id = id;
        this.isSwitchedOn = isSwitchedOn;
    }

    public int getId() {
        return id;
    }

    public LightSensorModel setId(int id) {
        this.id = id;
        return this;
    }


    public boolean isSwitchedOn() {
        return isSwitchedOn;
    }

    public LightSensorModel setSwitchedOn(boolean switchedOn) {
        isSwitchedOn = switchedOn;
        return this;
    }
}
