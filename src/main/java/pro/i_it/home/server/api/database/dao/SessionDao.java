package pro.i_it.home.server.api.database.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pro.i_it.home.server.api.database.dao.interfaces.ISessionDao;
import pro.i_it.home.server.api.database.entities.HttpSessionDbModel;

public class SessionDao implements ISessionDao {

    private SessionFactory sessionFactory;

    public SessionDao(SessionFactory session) {
        this.sessionFactory = session;
    }

    @Override
    public HttpSessionDbModel createSession(int userId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        HttpSessionDbModel httpSessionDbModel = new HttpSessionDbModel(userId);
        session.save(httpSessionDbModel);
        session.flush();
        session.getTransaction().commit();
        session.close();
        return httpSessionDbModel;
    }

    @Override
    public int deleteSession(String sessionId) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query query = session.createQuery("delete HttpSessionDbModel " +
                "where sessionId = :sessionId");
        query.setParameter("sessionId", sessionId);
        int result = query.executeUpdate();
        session.flush();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    @Override
    public boolean isSessionExist(String uuid) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select h.sessionId from HttpSessionDbModel h where h.sessionId = :uuid");
        query.setParameter("uuid", uuid);
        boolean result = !query.list().isEmpty();
        session.close();
        return result;
    }
}
