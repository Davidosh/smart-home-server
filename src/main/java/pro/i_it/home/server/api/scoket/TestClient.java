package pro.i_it.home.server.api.scoket;

import pro.i_it.home.server.api.scoket.packets.interfaces.InPacket;
import pro.i_it.home.server.api.scoket.packets.interfaces.OutPacket;
import pro.i_it.home.server.api.scoket.packets.message.MessagePacketIn;
import pro.i_it.home.server.api.scoket.packets.message.MessagePacketOut;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TestClient {
    private Socket socket;
    private DataOutputStream outputStreamWriter;
    private DataInputStream dataInputStream;

    public TestClient(Socket socket) throws IOException {
        this.socket = socket;
        outputStreamWriter = new DataOutputStream(socket.getOutputStream());
        dataInputStream = new DataInputStream(socket.getInputStream());
    }

    public void work() {
        try {
            while (dataInputStream.available() > 0) {
                int packetId = dataInputStream.readInt();
                if (packetId == InPacket.PACKAGE_A) {
                    //MessagePacketIn messagePacketIn = new MessagePacketIn().readSelf(dataInputStream);
                    System.out.println("PACKAGE_A");
                    MessagePacketOut messagePacketOut = new MessagePacketOut();
                    messagePacketOut.sendA(outputStreamWriter);
                } else if (packetId == InPacket.PACKAGE_B) {
                    //MessagePacketIn messagePacketIn = new MessagePacketIn().readSelf(dataInputStream);
                    /*System.out.println(messagePacketIn.getMessage());*/
                    System.out.println("PACKAGE_B");
                    MessagePacketOut messagePacketOut = new MessagePacketOut();
                    messagePacketOut.sendB(outputStreamWriter);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isActive() {
        return socket.isConnected() && !socket.isClosed();
    }
}
