package pro.i_it.home.server.api.scoket;

import java.util.ArrayList;
import java.util.List;

public class ClientManager extends Thread {
    private List<TestClient> allClients = new ArrayList<>();
    private final List<TestClient> newClients = new ArrayList<>();
    private List<TestClient> removeClients = new ArrayList<>();

    public ClientManager() {
        start();
    }

    public void addNewClient(TestClient client) {
        synchronized (allClients) {
            newClients.add(client);
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            synchronized (allClients) {
                allClients.addAll(newClients);
                newClients.clear();
            }
            for (TestClient client : allClients) {
                if (client.isActive()) {
                    client.work();
                } else {
                    removeClients.add(client);
                }
            }
            allClients.removeAll(removeClients);
            removeClients.clear();
        }
    }
}
