package pro.i_it.home.server.api.web.servlets;

import pro.i_it.home.server.api.Context;
import pro.i_it.home.server.api.interactors.AuthInteractor;
import pro.i_it.home.server.api.retrofit.responces.LightResponceModel;
import pro.i_it.home.server.api.retrofit.responces.SwitchLedResponceRaspberryModel;
import pro.i_it.home.server.api.retrofit.responces.TemperatureResponceModel;
import pro.i_it.home.server.api.retrofit.responces.TemperatureResponceRaspberryModel;
import pro.i_it.home.server.api.web.model.BaseResponceModel;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static pro.i_it.home.server.api.Context.getContext;
import static pro.i_it.home.server.api.web.errors.ErrorContants.*;
import static pro.i_it.home.server.api.web.servlets.AuthServlet.SESSION_ID;
import static pro.i_it.home.server.api.web.servlets.interfaces.ApiContants.*;

public class DeviceServlet extends HttpServlet {


    private AuthInteractor authInteractor;

    public DeviceServlet() {
        authInteractor = Context.getContext().getInteractorsManager().getAuthInteractor();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (!isExistSession(req.getParameter(SESSION_ID))) {
            throwError(resp, NOT_AUTHORIZED);
            return;
        }
        int deviceType = Integer.parseInt(req.getParameter(DEVICE_TYPE));
        if (req.getParameterMap().containsKey(DEVICE_TYPE)) {
            handlePost(req, resp, deviceType);
        } else {
            throwError(resp, NO_DEVICE_TYPE);
        }
    }

    private void handlePost(HttpServletRequest req, HttpServletResponse resp, int deviceType) throws IOException {
        switch (deviceType) {
            case LED_SWITCH_CODE:
                switchLedDevice(req, resp, deviceType);
                break;
        }
    }

    private void switchLedDevice(HttpServletRequest req, HttpServletResponse resp, int deviceType) throws IOException {
        if (!req.getParameterMap().containsKey(TURN_ON) ||
                !req.getParameterMap().containsKey(LED_DEVICE)) {
            throwError(resp, NO_DATA);
        }
        SwitchLedResponceRaspberryModel switchLedResponceRaspberryModel = getContext().getLightRetrofitApi().switchLight(deviceType,
                Boolean.parseBoolean(req.getParameter(TURN_ON)),
                Integer.parseInt(req.getParameter(LED_DEVICE))).blockingGet();
        LightResponceModel lightResponceModel = new LightResponceModel();
        lightResponceModel.setTurnOn(switchLedResponceRaspberryModel.getData().isTurnOn());
        resp.getWriter().append(new BaseResponceModel<LightResponceModel>().setData(lightResponceModel).toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (!isExistSession(req.getParameter(SESSION_ID))) {
            throwError(resp, NOT_AUTHORIZED);
            return;
        }
        if (!req.getParameterMap().containsKey(DEVICE_TYPE)) {
            throwError(resp, NO_DEVICE_TYPE);
            return;
        }
        switch (Integer.parseInt(req.getParameter(DEVICE_TYPE))) {
            case GET_TEMPERATURE_CODE:
                getTemperature(resp);
                break;
            case GET_LED_STATUS_CODE:
                getLedStatus(req, resp);
                break;
            default:
                throwError(resp, INVALID_REQUEST);
        }
    }

    private boolean isExistSession(String sessionId) throws IOException {
        return authInteractor.isSessionExist(sessionId);
    }

    private void getLedStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (!req.getParameterMap().containsKey(LED_DEVICE)) {
            throwError(resp, NO_DATA);
            return;
        }
        SwitchLedResponceRaspberryModel switchLedResponceRaspberryModel =
                getContext().getLightRetrofitApi().getCurrentLight(GET_LED_STATUS_CODE,
                        Integer.parseInt(req.getParameter(LED_DEVICE))).blockingGet();
        LightResponceModel lightResponceModel = new LightResponceModel();
        lightResponceModel.setTurnOn(switchLedResponceRaspberryModel.getData().isTurnOn());
        resp.getWriter().append(new BaseResponceModel<LightResponceModel>()
                .setData(lightResponceModel).toString());
    }

    private void getTemperature(HttpServletResponse resp) throws IOException {
        TemperatureResponceRaspberryModel temperatureResponceRaspberryModel =
                getContext().getTemperatureSensorRetrofitApi().getTemp(GET_TEMPERATURE_CODE).blockingGet();
        TemperatureResponceModel temperatureResponceModel = new TemperatureResponceModel();
        temperatureResponceModel.setCurrentTemperature(temperatureResponceRaspberryModel.getData().getCurrentTemperature());
        resp.getWriter().append(new BaseResponceModel<TemperatureResponceModel>()
                .setData(temperatureResponceModel).toString());
    }

    private void throwError(HttpServletResponse resp, String errorText) throws IOException {
        resp.getWriter().append(new BaseResponceModel<>().setErrorDescription(errorText).toString());
    }
}
