package pro.i_it.home.server.api.web.model.devices;

public class ChangeStateModel<NewState> {
    private int id;
    private NewState newStateModel;

    public int getId() {
        return id;
    }

    public ChangeStateModel setId(int id) {
        this.id = id;
        return this;
    }

    public NewState getNewStateModel() {
        return newStateModel;
    }

    public ChangeStateModel setNewStateModel(NewState newStateModel) {
        this.newStateModel = newStateModel;
        return this;
    }
}
