package pro.i_it.home.server.api.database;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import pro.i_it.home.server.api.database.entities.AuthDbModel;
import pro.i_it.home.server.api.database.entities.HttpSessionDbModel;

public class DbSession {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) {
            return sessionFactory;
        }

        Configuration config = new Configuration();

        config.addAnnotatedClass(AuthDbModel.class);
        config.addAnnotatedClass(HttpSessionDbModel.class);

        config.setProperty("hibernate.connection.autoReconnect", "true");
        config.setProperty("connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
        config.setProperty("c3p0.min_size", "1");
        config.setProperty("c3p0.max_size", "1");
        config.setProperty("c3p0.timeout", "1800");
        config.setProperty("c3p0.max_statements", "2");
        config.setProperty("hibernate.c3p0.testConnectionOnCheckout", "true");
        config.setProperty("hibernate.hbm2ddl.auto", "update");
        config.setProperty("database.driver", "org.postgresql.Driver");
        config.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres");
        config.setProperty("hibernate.connection.username", "postgres");
        config.setProperty("hibernate.connection.password", "1111");
        config.setProperty("hibernate.current_session_context_class", "thread");
        config.setProperty("hibernate.enable_lazy_load_no_trans", "true");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }
}
