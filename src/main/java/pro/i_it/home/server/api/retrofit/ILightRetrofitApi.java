package pro.i_it.home.server.api.retrofit;

import io.reactivex.Single;
import pro.i_it.home.server.api.retrofit.responces.SwitchLedResponceRaspberryModel;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ILightRetrofitApi {
    @GET("/pin")
    Single<SwitchLedResponceRaspberryModel> getCurrentLight(@Query("deviceType") int deviceType,
                                                            @Query("ledDevice") int ledDevice);

    @POST("/pin")
    Single<SwitchLedResponceRaspberryModel> switchLight(@Query("deviceType") int deviceType,
                                                        @Query("turnOn") boolean turnOn,
                                                        @Query("ledDevice") int ledDevice);
}
