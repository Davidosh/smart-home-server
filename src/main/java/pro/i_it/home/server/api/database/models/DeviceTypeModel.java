package pro.i_it.home.server.api.database.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class DeviceTypeModel implements Serializable {
    @Expose
    private int id;
    @Expose
    private int deviceId;
    @Expose
    private String description;

    public DeviceTypeModel(int id, int deviceId, String description) {
        this.id = id;
        this.deviceId = deviceId;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public DeviceTypeModel setId(int id) {
        this.id = id;
        return this;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public DeviceTypeModel setDeviceId(int deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public DeviceTypeModel setDescription(String description) {
        this.description = description;
        return this;
    }
}
