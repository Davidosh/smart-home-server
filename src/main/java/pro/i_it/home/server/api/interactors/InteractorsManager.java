package pro.i_it.home.server.api.interactors;

import pro.i_it.home.server.api.Context;

public class InteractorsManager {
    private AuthInteractor authInteractor;

    public InteractorsManager(Context context) {
        authInteractor = new AuthInteractor();
        authInteractor.setSessionDao(context.getDataManager().getSessionDao());
        authInteractor.setUserDao(context.getDataManager().getUserDao());

    }

    public AuthInteractor getAuthInteractor() {
        return authInteractor;
    }

 }
