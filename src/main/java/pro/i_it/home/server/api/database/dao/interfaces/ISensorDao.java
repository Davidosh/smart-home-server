package pro.i_it.home.server.api.database.dao.interfaces;

import io.reactivex.Completable;
import io.reactivex.Single;
import org.eclipse.jetty.util.Promise;
import pro.i_it.home.server.api.database.entities.SensorDbModel;
import pro.i_it.home.server.api.database.models.SensorModel;

import java.util.List;

public interface ISensorDao {
    Completable insertSensor(SensorModel sensorModel);

    Completable deleteSensor(int id);

    Completable updateSensor(SensorModel sensorModel);

    Single<SensorModel> getSensor(int id);

    Single<List<SensorModel>> getAllSensors();
}
