package pro.i_it.home.server.api.web.model.devices;

public class TemperatureSensorModel {
    private int id;
    private String deviceId;
    private float value;

    public int getId() {
        return id;
    }

    public TemperatureSensorModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public TemperatureSensorModel setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public float getValue() {
        return value;
    }

    public TemperatureSensorModel setValue(float value) {
        this.value = value;
        return this;
    }
}
