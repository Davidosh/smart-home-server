package pro.i_it.home.server.api.database.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sensor")
public class SensorDbModel implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "device_id")
    private String deviceId;
    @Column(name = "device_type_id")
    private int deviceTypeId;
    @Column(name = "last_server_ip")
    private String lastServerIp;
    @Column(name = "enabled")
    private boolean enabled;
    @Column(name = "description")
    private String description;

    public SensorDbModel() {
    }

    public SensorDbModel(String deviceId, int deviceTypeId, String lastServerIp, boolean enabled, String description) {
        this.deviceId = deviceId;
        this.deviceTypeId = deviceTypeId;
        this.lastServerIp = lastServerIp;
        this.enabled = enabled;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(int deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public String getLastServerIp() {
        return lastServerIp;
    }

    public void setLastServerIp(String lastServerIp) {
        this.lastServerIp = lastServerIp;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
