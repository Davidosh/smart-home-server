package pro.i_it.home.server.api.database;

import org.hibernate.SessionFactory;
import pro.i_it.home.server.api.database.dao.interfaces.ISessionDao;
import pro.i_it.home.server.api.database.dao.interfaces.IUserDao;
import pro.i_it.home.server.api.database.dao.SessionDao;
import pro.i_it.home.server.api.database.dao.UserDao;

public class DataManager {
    private SessionFactory sessionFactory;
    private ISessionDao sessionDao;
    private IUserDao userDao;

    public DataManager() {
        sessionFactory = DbSession.getSessionFactory();
        sessionDao = new SessionDao(sessionFactory);
        userDao = new UserDao(sessionFactory);
    }

    public ISessionDao getSessionDao() {
        return sessionDao;
    }

    public IUserDao getUserDao() {
        return userDao;
    }
}
