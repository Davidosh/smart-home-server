package pro.i_it.home.server.api.web.errors;

public interface ErrorContants {
    String NO_DEVICE_TYPE = "No device type field";
    String NOT_AUTHORIZED = "Not authorized";
    String NO_DATA = "Not all data";
    String INVALID_REQUEST = "Invalid request";
}
