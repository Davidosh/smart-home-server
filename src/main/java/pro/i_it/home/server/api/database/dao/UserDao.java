package pro.i_it.home.server.api.database.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pro.i_it.home.server.api.database.dao.interfaces.IUserDao;
import pro.i_it.home.server.api.database.models.UserModel;


public class UserDao implements IUserDao {
    private SessionFactory sessionFactory;

    private static final ThreadLocal<Session> threadLocal = new ThreadLocal<Session>();

    public UserDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserModel getUserByUuidSession(String uuid) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select new pro.i_it.home.server.api.database.models.UserModel(a.authId, a.nickname, a.password)" +
                " from AuthDbModel a, HttpSessionDbModel s where a.authId = s.userId  and s.sessionId = :uuid");
        query.setParameter("uuid", uuid);
        UserModel userModel = query.list().isEmpty() ? null : (UserModel) query.list().get(0);
        session.close();
        return userModel;
    }

    @Override
    public int getUserIdByLoginAndPassword(String login, String password) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("select a.authId from AuthDbModel a " +
                "where a.nickname = :nickname and a.password = :password");
        query.setParameter("nickname", login);
        query.setParameter("password", password);
        int userId = query.list().isEmpty() ? 0 : (int) query.list().get(0);
        session.close();
        return userId;
    }

    Session getSession() {
        Session session = threadLocal.get();
        if (session != null) {
            return session;
        }

        session = sessionFactory.openSession();
        threadLocal.set(session);

        return session;
    }
}