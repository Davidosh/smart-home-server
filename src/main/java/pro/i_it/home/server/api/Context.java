package pro.i_it.home.server.api;

import pro.i_it.home.server.api.database.DataManager;
import pro.i_it.home.server.api.interactors.InteractorsManager;
import pro.i_it.home.server.api.retrofit.ILightRetrofitApi;
import pro.i_it.home.server.api.retrofit.ITemperatureSensorRetrofitApi;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Context {
    private static Context context;
    private DataManager dataManager;
    private InteractorsManager interactorsManager;
    private Retrofit retrofit;
    private ILightRetrofitApi lightRetrofitApi;
    private ITemperatureSensorRetrofitApi temperatureSensorRetrofitApi;

    private final String RASPBERRY_ENDPOINT = "http://192.168.43.149:8081";


    private Context() {
        dataManager = new DataManager();
        interactorsManager = new InteractorsManager(this);
        retrofit = new Retrofit.Builder()
                .baseUrl(RASPBERRY_ENDPOINT) //Базовая часть адреса
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        lightRetrofitApi = retrofit.create(ILightRetrofitApi.class);
        temperatureSensorRetrofitApi = retrofit.create(ITemperatureSensorRetrofitApi.class);


    }

    public static Context getContext() {
        if (context == null) {
            synchronized (Context.class) {
                if (context == null) {
                    context = new Context();
                }
            }
        }
        return context;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public InteractorsManager getInteractorsManager() {
        return interactorsManager;
    }

    public ILightRetrofitApi getLightRetrofitApi() {
        return lightRetrofitApi;
    }

    public ITemperatureSensorRetrofitApi getTemperatureSensorRetrofitApi() {
        return temperatureSensorRetrofitApi;
    }
}
