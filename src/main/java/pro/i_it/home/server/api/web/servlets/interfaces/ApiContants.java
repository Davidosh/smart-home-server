package pro.i_it.home.server.api.web.servlets.interfaces;

public interface ApiContants {
    int LED_SWITCH_CODE = 2;

    int GET_TEMPERATURE_CODE = 1;
    int GET_LED_STATUS_CODE = 2;

    String DEVICE_TYPE = "deviceType";
    String TURN_ON = "turnOn";
    String LED_DEVICE = "ledDevice";
}
