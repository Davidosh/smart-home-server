package pro.i_it.home.server.api.interactors;

import pro.i_it.home.server.api.database.dao.interfaces.ISessionDao;
import pro.i_it.home.server.api.database.dao.interfaces.IUserDao;
import pro.i_it.home.server.api.database.entities.HttpSessionDbModel;

public class AuthInteractor {

    private IUserDao userDao;
    private ISessionDao sessionDao;


    public AuthInteractor() {
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }

    public void setSessionDao(ISessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public String signIn(String login, String password) {
        int userId = userDao.getUserIdByLoginAndPassword(login, password);
        if (userId != 0) {
            HttpSessionDbModel httpSessionDbModel = sessionDao.createSession(userId);
            return httpSessionDbModel.getSessionId();
        }
        return null;
    }

    public int signOut(String sessionId) {
        return sessionDao.deleteSession(sessionId);
    }
    public boolean isSessionExist(String uuid){
        return sessionDao.isSessionExist(uuid);
    }
}
