package pro.i_it.home.server.api.scoket.packets.message;

import pro.i_it.home.server.api.scoket.packets.interfaces.InPacket;

import java.io.DataInputStream;
import java.io.IOException;

public class MessagePacketIn implements InPacket<MessagePacketIn> {
    private int message;

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    @Override
    public MessagePacketIn readSelf(DataInputStream dataOutputStream) {
        MessagePacketIn messagePacketIn = new MessagePacketIn();
        try {

            messagePacketIn.message = dataOutputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return messagePacketIn;
    }
}
