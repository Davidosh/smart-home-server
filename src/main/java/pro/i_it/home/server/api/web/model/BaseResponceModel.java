package pro.i_it.home.server.api.web.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class BaseResponceModel<Data>{
    private static final int OK = 1;
    private static final int UNKNOWN_ERROR = -1;
    @Expose
    private int responceCode;
    @Expose
    private Data data;
    @Expose
    private String errorDescription;

    public BaseResponceModel(Data data) {
        this.data = data;
        responceCode = OK;
    }

    public BaseResponceModel() {
        responceCode = OK;
    }

    @Override
    public String toString() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
    }

    public int getResponceCode() {
        return responceCode;
    }

    public BaseResponceModel setResponceCode(int responceCode) {
        this.responceCode = responceCode;
        return this;
    }

    public Data getData() {
        return data;
    }

    public BaseResponceModel setData(Data data) {
        this.data = data;
        return this;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public BaseResponceModel setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
        responceCode = UNKNOWN_ERROR;
        return this;
    }
}
