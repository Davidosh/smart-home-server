package pro.i_it.home.server.api.scoket.packets.interfaces;

import java.io.DataOutputStream;

public interface OutPacket {
    int PACKAGE_A = 1;
    int PACKAGE_B = 2;
    int PACKAGE_C = 3;
    int PACKAGE_D = 4;

    void sendA(DataOutputStream outputStreamWriter);

    void sendB(DataOutputStream outputStreamWriter);
}
