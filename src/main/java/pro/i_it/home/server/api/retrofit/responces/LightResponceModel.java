package pro.i_it.home.server.api.retrofit.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LightResponceModel {
    @Expose
    private boolean turnOn;

    public boolean isTurnOn() {
        return turnOn;
    }

    public void setTurnOn(boolean turnOn) {
        this.turnOn = turnOn;
    }
}
