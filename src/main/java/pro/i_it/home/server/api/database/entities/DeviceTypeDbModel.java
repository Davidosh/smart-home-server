package pro.i_it.home.server.api.database.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "device_type")
public class DeviceTypeDbModel implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "device_id")
    private int deviceId;
    @Column(name = "description")
    private String description;

    public DeviceTypeDbModel() {
    }

    public DeviceTypeDbModel(int deviceId, String description) {
        this.deviceId = deviceId;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
